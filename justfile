#!/usr/bin/env just --justfile

m:
    PORT=8080 cargo run --bin replicate-log-master
r1:
    PORT=50501 cargo run --bin replicate-log-secondary
r2:
    PORT=50502 cargo run --bin replicate-log-secondary