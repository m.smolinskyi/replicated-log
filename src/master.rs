use std::env;

use axum::Router;

use std::net::SocketAddr;

use crate::model::ModelController;
use crate::replicate_client::client::ReplicateController;
use routes::routes_messages::routes;

mod error;
mod model;
mod replicate_client;
mod routes;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let port = env::var("PORT").unwrap_or("8080".to_owned());

    let replicate_first_port = env::var("REPLICATE_FIRST_PORT").unwrap_or("50501".to_owned());
    let replicate_second_port = env::var("REPLICATE_SECOND_PORT").unwrap_or("50502".to_owned());

    let replicate_first_host = env::var("REPLICATE_FIRST_HOST").unwrap_or("127.0.0.1".to_owned());
    let replicate_second_host = env::var("REPLICATE_SECOND_HOST").unwrap_or("127.0.0.1".to_owned());

    // Initialize ModelController
    let model_controller = ModelController::new().await?;
    // Initialize ReplicateController
    let replicate_controller = ReplicateController::new(
        (
            replicate_first_port.parse::<u16>().unwrap(),
            replicate_second_port.parse::<u16>().unwrap(),
        ),
        (replicate_first_host, replicate_second_host),
    )
    .await?;

    let routes_master = Router::new().merge(routes(
        model_controller.clone(),
        replicate_controller.clone(),
    ));

    let addr = SocketAddr::from(([0, 0, 0, 0], port.parse::<u16>().unwrap()));

    println!("[SYSTEM] HTTP SERVER LISTENING ON PORT {addr}\n");

    axum::Server::bind(&addr)
        .serve(routes_master.into_make_service())
        .await
        .unwrap();

    Ok(())
}
