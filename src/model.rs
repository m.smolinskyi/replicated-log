use std::sync::{Arc, Mutex};

use super::error::Result;

#[derive(Clone)]
pub struct ModelController {
    messages: Arc<Mutex<Vec<Option<String>>>>,
}

impl ModelController {
    pub async fn new() -> Result<Self> {
        Ok(Self {
            messages: Arc::default(),
        })
    }
}

impl ModelController {
    pub async fn add_message(&self, message: String) -> Result<String> {
        let mut messages = self.messages.lock().unwrap();

        messages.push(Some(message.clone()));

        Ok(message)
    }

    pub async fn messages(&self) -> Result<Vec<String>> {
        let store = self.messages.lock().unwrap();
        let messages = store.iter().filter_map(|message| message.clone()).collect();

        Ok(messages)
    }
}
