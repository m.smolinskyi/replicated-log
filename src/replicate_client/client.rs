use tonic::Response;
use tonic::transport::Channel;

use messages::{MessageRequest, ReplicateMessagesRequest, ReplicateMessagesResponse};
use messages::replicate_client::ReplicateClient;

pub use crate::error::Result;

use self::messages::MessageResponse;

pub mod messages {
    tonic::include_proto!("messages");
}

#[derive(Clone)]
pub struct ReplicateController {
    client1: ReplicateClient<Channel>,
    client2: ReplicateClient<Channel>,
}

impl ReplicateController {
    pub async fn new(
        replicate_ports: (u16, u16),
        replicate_hosts: (String, String),
    ) -> Result<Self> {
        let client1_address =
            format!("http://{}:{}", replicate_hosts.0, replicate_ports.0).to_string();

        let client2_address =
            format!("http://{}:{}", replicate_hosts.1, replicate_ports.1).to_string();

        let client1 = ReplicateClient::connect(client1_address)
            .await
            .map_err(|_| eprintln!("[ERROR]: unable to connect replicate server 1"))
            .unwrap();

        let client2 = ReplicateClient::connect(client2_address)
            .await
            .map_err(|_| eprintln!("[ERROR]: unable to connect replicate server 2"))
            .unwrap();

        Ok(Self { client1, client2 })
    }
}

impl ReplicateController {
    pub async fn replicate_message(
        &mut self,
        message: String,
    ) -> Result<Response<MessageResponse>> {
        let request1 = tonic::Request::new(MessageRequest {
            message: message.clone(),
        });

        let request2 = tonic::Request::new(MessageRequest {
            message: message.clone(),
        });

        let (response1, _response2) = tokio::join!(
            self.client1.send_message(request1),
            self.client2.send_message(request2)
        );

        let total_response = response1
            .map_err(|_| eprintln!("[ERROR]: unable to replicate message"))
            .unwrap();

        Ok(total_response)
    }

    pub async fn replicated_messages(&mut self) -> Result<Response<ReplicateMessagesResponse>> {
        let request1 = tonic::Request::new(ReplicateMessagesRequest {});
        let request2 = tonic::Request::new(ReplicateMessagesRequest {});

        let (response1, _response2) = tokio::join!(
            self.client1.messages(request1),
            self.client2.messages(request2)
        );

        let total_response = response1
            .map_err(|_| eprintln!("[ERROR]: unable to retrieve replicated messages"))
            .unwrap();

        Ok(total_response)
    }
}
