use std::sync::{Arc, Mutex};

use tonic::{Request, Response, Status};

use messages::replicate_server::Replicate;
use messages::{
    MessageRequest, MessageResponse, ReplicateMessagesRequest, ReplicateMessagesResponse,
};

pub mod messages {
    tonic::include_proto!("messages");
}

#[derive(Debug, Default)]
pub struct MessagesService {
    store: Arc<Mutex<Vec<Option<String>>>>,
}

impl MessagesService {
    pub fn new() -> Self {
        MessagesService {
            store: Arc::default(),
        }
    }
}

#[tonic::async_trait]
impl Replicate for MessagesService {
    async fn send_message(
        &self,
        request: Request<MessageRequest>,
    ) -> Result<Response<MessageResponse>, Status> {
        let req = request.into_inner();

        let mut store = self.store.lock().unwrap();
        let message = Some(req.clone().message);

        store.push(message.clone());

        println!(
            "[REPLICATE:HANDLER] send_message: message successfully replicated: {}",
            message.unwrap()
        );

        let reply = MessageResponse { successful: true };

        Ok(Response::new(reply))
    }

    async fn messages(
        &self,
        _: Request<ReplicateMessagesRequest>,
    ) -> Result<Response<ReplicateMessagesResponse>, Status> {
        let store = self.store.lock().unwrap();

        let reply = ReplicateMessagesResponse {
            messages: store.iter().filter_map(|message| message.clone()).collect(),
        };

        Ok(Response::new(reply))
    }
}
