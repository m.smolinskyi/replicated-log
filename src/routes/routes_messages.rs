use crate::error::Result;
use crate::model::ModelController;
use crate::replicate_client::client::ReplicateController;
use axum::{extract::State, routing::post, Json, Router};

#[derive(Clone)]
pub struct AppState {
    model_controller: ModelController,
    replicate_controller: ReplicateController,
}

pub fn routes(
    model_controller: ModelController,
    replicate_controller: ReplicateController,
) -> Router {
    let app_state = AppState {
        model_controller,
        replicate_controller,
    };

    Router::new()
        .route("/", post(create_message).get(messages))
        .with_state(app_state.clone())
}

async fn create_message(
    State(app_state): State<AppState>,
    Json(message): Json<String>,
) -> Result<Json<String>> {
    let mc = app_state.model_controller;
    let mut rc = app_state.replicate_controller;

    let message = mc.add_message(message).await?;

    rc.replicate_message(message.clone()).await.unwrap();
    println!(
        "[MASTER:HANDLER] create_message: message successfully replicated: {}",
        message
    );

    Ok(Json(message))
}

async fn messages(State(app_state): State<AppState>) -> Result<Json<Vec<String>>> {
    let mc = app_state.model_controller;
    let mut rc = app_state.replicate_controller;

    let messages = mc.messages().await?;

    let replicated_messages = rc.replicated_messages().await.unwrap();

    println!(
        "[MASTER:HANDLER] messages: replicated messages synced: {:?}",
        replicated_messages.get_ref().messages
    );

    Ok(Json(messages))
}
