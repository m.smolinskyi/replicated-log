use std::env;
use std::net::SocketAddr;
use tonic::transport::Server;

use crate::replicate_server::replicate_server::MessagesService;
use crate::replicate_server::replicate_server::messages::replicate_server::ReplicateServer;

mod replicate_server;


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let port = env::var("PORT")
        .map_err(|_| eprintln!("[ERROR]: unable read PORT (environment variable not found)"))
        .unwrap();

    let addr = SocketAddr::from(([0, 0, 0, 0], port.parse::<u16>().unwrap()));

    let messages_service = MessagesService::new();

    println!("[SYSTEM] REPLICATE SERVER LISTENING ON PORT {addr}\n");

    Server::builder()
        .add_service(ReplicateServer::new(messages_service))
        .serve(addr)
        .await?;

    Ok(())
}
